﻿using Core.Areas.LibreNms.Models;
using Core.Areas.LibreNms.Models.Device;
using LibreNms.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("LibreNms")]
    public class DeviceController : Controller
    {
        private readonly AppSettings _appSettings;

        public DeviceController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<IActionResult> List()
        {
            try
            {
                LibreNmsClient libreNmsClient = new LibreNmsClient(_appSettings.EndpointCurrentVersionUrl(), _appSettings.ApiSecret);
                ListViewModel listViewModel = new ListViewModel();
                listViewModel.Devices = new List<DeviceModel>();

                List<string> devicesToRetrive = new List<string>(){ "10", "12", "14", "21", "22" };
                foreach(string deviceId in devicesToRetrive)
                {
                    global::LibreNms.Client.Models.Devices.Get.ResponseModel responseModelDevice = await libreNmsClient.GetDeviceDetailsByIdAsync(deviceId);
                    DeviceModel viewModelDevice = new DeviceModel();
                    viewModelDevice.SysName = responseModelDevice.Devices[0].SysName;
                    viewModelDevice.Purpose = responseModelDevice.Devices[0].Purpose;
                    viewModelDevice.LastPolled = responseModelDevice.Devices[0].LastPolled;
                    if (responseModelDevice.Devices[0].Uptime != null)
                    {                   
                        TimeSpan t = TimeSpan.FromSeconds(responseModelDevice.Devices[0].Uptime.Value);
                        viewModelDevice.Uptime = string.Format("{0} Days, {1} Hours, {2} Minutes, {3} Seconds", t.Days, t.Hours, t.Minutes, t.Seconds);
                    }
                    viewModelDevice.Status = responseModelDevice.Devices[0].Status;
                    listViewModel.Devices.Add(viewModelDevice);
                }

                return View(listViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}
