using Core.Areas.LibreNms.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Controllers
{
    [Area("LibreNms")]
    public class HomeController : Controller
    {
        private readonly AppSettings _appSettings;

        public HomeController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IActionResult Index()
        {
            IndexViewModel indexViewModel = new IndexViewModel();
            indexViewModel.ApiUrl = _appSettings.ApiUrl;
            indexViewModel.ApiVersion = _appSettings.ApiVersion;
            return View(indexViewModel);
        }
    }
}