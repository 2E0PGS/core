﻿using Core.Areas.LibreNms.Models;
using Core.Areas.LibreNms.Models.Service;
using LibreNms.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("LibreNms")]
    public class ServiceController : Controller
    {
        private readonly AppSettings _appSettings;

        public ServiceController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<IActionResult> List()
        {
            try
            {
                LibreNmsClient libreNmsClient = new LibreNmsClient(_appSettings.EndpointCurrentVersionUrl(), _appSettings.ApiSecret);
                ListViewModel listViewModel = new ListViewModel();
                listViewModel.Services = new List<ServiceModel>();

                List<string> servicesToRetrive = new List<string>(){ "10", "12", "21", "22" };
                foreach(string serviceId in servicesToRetrive)
                {
                    global::LibreNms.Client.Models.Services.Get.ResponseModel responseModelService = await libreNmsClient.GetServicesByIdAsync(serviceId);
                    foreach(global::LibreNms.Client.Models.Services.Get.ServiceModel service in responseModelService.Services[0])
                    {
                        ServiceModel viewModelService = new ServiceModel();
                        viewModelService.ServiceType = service.ServiceType;
                        viewModelService.ServiceDesc = service.ServiceDesc;
                        viewModelService.ServiceMessage = service.ServiceMessage;
                        viewModelService.ServiceStatus = service.ServiceStatus;
                        
                        listViewModel.Services.Add(viewModelService);
                    }
                }

                return View(listViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }     
    }
}