﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models
{
    public class IndexViewModel
    {
        public string ApiUrl { get; set; }
        public int ApiVersion { get; set; }
    }
}
