﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models.Device
{
    public class ListViewModel
    {
        public List<DeviceModel> Devices { get; set; }
    }
}
