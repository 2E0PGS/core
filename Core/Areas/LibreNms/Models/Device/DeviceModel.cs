using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models.Device
{
    public class DeviceModel {
        public string SysName { get; set; }
        public string Purpose { get; set; }
        public DateTime? LastPolled { get; set; }
        public string Uptime { get; set; }
        public int Status { get; set; }
    }
}