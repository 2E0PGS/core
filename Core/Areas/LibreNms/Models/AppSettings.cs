﻿// Model to store AppSettings for Overham. This gets populated by ConfigureServices.

namespace Core.Areas.LibreNms.Models
{
    public class AppSettings
    {
        public string ApiUrl { get; set; }
        public int ApiVersion { get; set; }

        public string ApiSecret { get; set; }

        public string EndpointCurrentVersionUrl()
        {
            return ApiUrl + "/v" + ApiVersion;
        }
    }
}
