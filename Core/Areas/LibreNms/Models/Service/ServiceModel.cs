using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models.Service
{
    public class ServiceModel
    {
        public string ServiceType { get; set; }
        public string ServiceDesc { get; set; }
        public string ServiceMessage { get; set; }
        public int ServiceStatus { get; set; }
    }
}