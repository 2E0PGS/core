﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models.Service
{
    public class ListViewModel
    {
        public List<ServiceModel> Services { get; set; }
    }
}
