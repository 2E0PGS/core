﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.LibreNms.Models
{
    public class ErrorViewModel
    {
        public string Message { get; set; }
    }
}
