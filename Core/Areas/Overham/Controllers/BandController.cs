﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.Band;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class BandController : Controller
    {
        private readonly AppSettings _appSettings;

        public BandController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IActionResult Search()
        {
            SearchViewModel searchViewModel = new SearchViewModel();
            return View(searchViewModel);
        }

        public async Task<IActionResult> SearchResults(SearchViewModel searchViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Band.Search.ResponseModel responseModel = await overhamClient.GetBandByFrequencyAsync(searchViewModel.FrequencyInHz);
                SearchResultsViewModel searchResultsViewModel = new SearchResultsViewModel();
                searchResultsViewModel.BandId = responseModel.Data;
                return View(searchResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public IActionResult Lookup()
        {
            LookupViewModel lookupViewModel = new LookupViewModel();
            return View(lookupViewModel);
        }

        public async Task<IActionResult> LookupResults(LookupViewModel lookupViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Band.Lookup.ResponseModel responseModel = await overhamClient.GetBandByIdAsync(lookupViewModel.BandId);
                LookupResultsViewModel lookupResultsViewModel = new LookupResultsViewModel();
                lookupResultsViewModel.Name = responseModel.Data.Name;
                lookupResultsViewModel.Country = responseModel.Data.Country;
                lookupResultsViewModel.Id = responseModel.Data.Id;
                lookupResultsViewModel.Lower = responseModel.Data.Lower;
                lookupResultsViewModel.Upper = responseModel.Data.Upper;
                return View(lookupResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}