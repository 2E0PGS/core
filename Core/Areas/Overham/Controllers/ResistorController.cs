﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.Resistor;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class ResistorController : Controller
    {
        private readonly AppSettings _appSettings;

        public ResistorController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IActionResult Encode()
        {
            EncodeViewModel encodeViewModel = new EncodeViewModel();
            return View(encodeViewModel);
        }

        public async Task<IActionResult> EncodeResults(EncodeViewModel encodeViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Resistor.Encode.ResponseModel responseModel = await overhamClient.GetResistorColorBandsAsync(encodeViewModel.Ohms);
                EncodeResultsViewModel encodeResultsViewModel = new EncodeResultsViewModel();
                encodeResultsViewModel.Band0Color = responseModel.Data.Band0str;
                encodeResultsViewModel.Band1Color = responseModel.Data.Band1str;
                encodeResultsViewModel.Band2Color = responseModel.Data.Band2str;
                return View(encodeResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}