﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.QCode;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class QCodeController : Controller
    {
        private readonly AppSettings _appSettings;

        public QCodeController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IActionResult Lookup()
        {
            LookupViewModel lookupViewModel = new LookupViewModel();
            return View(lookupViewModel);
        }

        public async Task<IActionResult> LookupResults(LookupViewModel lookupViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.QCode.Lookup.ResponseModel responseModel = await overhamClient.GetQCodeInfoAsync(lookupViewModel.Code);
                LookupResultsViewModel lookupResultsViewModel = new LookupResultsViewModel();
                lookupResultsViewModel.Tx = responseModel.Data.Tx;
                lookupResultsViewModel.Rx = responseModel.Data.Rx;
                lookupResultsViewModel.Authority = responseModel.Data.Authority;
                return View(lookupResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}