﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.Callsign;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class CallsignController : Controller
    {
        private readonly AppSettings _appSettings;

        public CallsignController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IActionResult Validate()
        {
            ValidateViewModel validateViewModel = new ValidateViewModel();
            return View(validateViewModel);
        }

        public async Task<IActionResult> ValidateResults(ValidateViewModel validateViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Callsign.Validate.ResponseModel responseModel = await overhamClient.GetCallsignValidationAsync(validateViewModel.Callsign);
                ValidateResultsViewModel validateResultsViewModel = new ValidateResultsViewModel();
                validateResultsViewModel.Valid = responseModel.Data;
                return View(validateResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public IActionResult Info()
        {
            InfoViewModel infoViewModel = new InfoViewModel();
            return View(infoViewModel);
        }

        public async Task<IActionResult> InfoResults(InfoViewModel infoViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Callsign.Info.ResponseModel responseModel = await overhamClient.GetCallsignInfoAsync(infoViewModel.Callsign);
                InfoResultsViewModel infoResultsViewModel = new InfoResultsViewModel();
                infoResultsViewModel.Region = responseModel.Data.Region;
                infoResultsViewModel.Country = responseModel.Data.Country;
                infoResultsViewModel.Callsign = responseModel.Data.Value;
                infoResultsViewModel.Type = responseModel.Data.Type;
                infoResultsViewModel.AmateurClass = responseModel.Data.AmateurClass.String;
                return View(infoResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}
