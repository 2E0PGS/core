﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.Recordings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class RecordingsController : Controller
    {
        private readonly AppSettings _appSettings;

        public RecordingsController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<IActionResult> StatsResults()
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Recordings.Stats.ResponseModel responseModel = await overhamClient.GetRecordingStatsAsync();
                StatsResultsViewModel statsResultsViewModel = new StatsResultsViewModel();
                statsResultsViewModel.Available = responseModel.Data.Available;
                statsResultsViewModel.Earliest = responseModel.Data.Earliest;
                statsResultsViewModel.LastDump = responseModel.Data.LastDump;
                statsResultsViewModel.Latest = responseModel.Data.Latest;
                statsResultsViewModel.ModeAmCount = responseModel.Data.ModeAmCount;
                statsResultsViewModel.ModeCWCount = responseModel.Data.ModeCWCount;
                statsResultsViewModel.ModeDVCount = responseModel.Data.ModeDVCount;
                statsResultsViewModel.ModeFmCount = responseModel.Data.ModeFmCount;
                statsResultsViewModel.ModeLsbCount = responseModel.Data.ModeLsbCount;
                statsResultsViewModel.ModeUsbCount = responseModel.Data.ModeUsbCount;
                statsResultsViewModel.TotalCount = responseModel.Data.TotalCount;
                statsResultsViewModel.UploadedCount = responseModel.Data.UploadedCount;
                return View(statsResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public IActionResult Info()
        {
            InfoViewModel infoViewModel = new InfoViewModel();
            return View(infoViewModel);
        }

        public async Task<IActionResult> InfoResults(InfoViewModel infoViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Recordings.Info.ResponseModel responseModel = await overhamClient.GetRecordingInfoAsync(infoViewModel.Id);
                InfoResultsViewModel infoResultsModel = new InfoResultsViewModel();
                infoResultsModel.Band = responseModel.Data.Band;
                infoResultsModel.Callsign = responseModel.Data.Callsign;
                infoResultsModel.Direction = responseModel.Data.Direction;
                infoResultsModel.HZFreq = responseModel.Data.HZFreq;
                infoResultsModel.Length = responseModel.Data.Length;
                infoResultsModel.Maidenhead = responseModel.Data.Maidenhead;
                infoResultsModel.Mode = responseModel.Data.Mode;
                infoResultsModel.Model = responseModel.Data.Model;
                infoResultsModel.WhenStart = responseModel.Data.WhenStart;
                return View(infoResultsModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public IActionResult Search()
        {
            SearchViewModel searchViewModel = new SearchViewModel();
            searchViewModel.StartTime = DateTime.Today;
            searchViewModel.EndTime = DateTime.Today;
            searchViewModel.StartHZ = "430825000";
            searchViewModel.EndHZ = "430850000";
            return View(searchViewModel);
        }

        public async Task<IActionResult> SearchResults(SearchViewModel searchViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Recordings.Search.ResponseModel responseModel = await overhamClient.GetRecordingsByRangeAsync(
                    searchViewModel.StartHZ,
                    searchViewModel.EndHZ, 
                    ((DateTimeOffset)searchViewModel.StartTime).ToUnixTimeSeconds().ToString(), 
                    ((DateTimeOffset)searchViewModel.EndTime).ToUnixTimeSeconds().ToString());

                SearchResultsViewModel searchResultsViewModel = new SearchResultsViewModel();
                searchResultsViewModel.ResultCount = responseModel.Data.ResultCount;
                // Just to avoid using AutoMapper, use some crazy Linq.
                searchResultsViewModel.Results = responseModel.Data.Results.Select(m => new ResultsModel() { Duration = m.Duration, HZFreq = m.HZFreq, Id = m.Id, WhenStart = m.WhenStart }).ToList();
                searchResultsViewModel.EndpointCurrentVersionUrl = _appSettings.EndpointCurrentVersionUrl();
                return View(searchResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}