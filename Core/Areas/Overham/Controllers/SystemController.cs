﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class SystemController : Controller
    {
        private readonly AppSettings _appSettings;

        public SystemController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<IActionResult> UptimeResults()
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.System.Uptime.ResponseModel responseModel = await overhamClient.GetSystemUptimeAsync();
                UptimeResultsViewModel uptimeResultsViewModel = new UptimeResultsViewModel();
                TimeSpan timeSpan = TimeSpan.FromSeconds(responseModel.Data.Seconds);
                uptimeResultsViewModel.Uptime = "Days: " + timeSpan.Days.ToString() + " Hours: " + timeSpan.Hours.ToString() + " Mins: " + timeSpan.Minutes.ToString() + " Seconds: " + timeSpan.Seconds.ToString();
                return View(uptimeResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("../Shared/Error");
            }
        }

        public async Task<IActionResult> PingResults()
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.System.Ping.ResponseModel responseModel = await overhamClient.GetSystemPingStatusAsync();
                PingResultsViewModel pingResultsViewModel = new PingResultsViewModel();
                pingResultsViewModel.Response = responseModel.Data;
                return View(pingResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("../Shared/Error");
            }
        }

        public IActionResult Error()
        {
            Models.System.ErrorViewModel errorViewModel = new Models.System.ErrorViewModel();
            return View(errorViewModel);
        }

        public async Task<IActionResult> ErrorResults(Models.System.ErrorViewModel errorViewModel)
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.System.Error.ResponseModel responseModel = await overhamClient.GetErrorInfoAsync(errorViewModel.Id);
                ErrorResultsViewModel errorResultsViewModel = new ErrorResultsViewModel();
                errorResultsViewModel.Symbolic = responseModel.Data.Symbolic;
                errorResultsViewModel.Message = responseModel.Data.Message;
                errorResultsViewModel.Id = responseModel.Data.Id;
                return View(errorResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("../Shared/Error");
            }
        }
    }
}