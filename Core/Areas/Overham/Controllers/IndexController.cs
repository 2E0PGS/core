﻿using Core.Areas.Overham.Models;
using Core.Areas.Overham.Models.Index;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Overham.Client;
using System;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Controllers
{
    [Area("Overham")]
    public class IndexController : Controller
    {
        private readonly AppSettings _appSettings;

        public IndexController(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public async Task<IActionResult> RoutesResults()
        {
            try
            {
                OverhamClient overhamClient = new OverhamClient(_appSettings.EndpointCurrentVersionUrl());
                global::Overham.Client.Models.Index.Routes.ResponseModel responseModel = await overhamClient.GetIndexRoutesAsync();
                RoutesResultsViewModel routesResultsViewModel = new RoutesResultsViewModel();
                routesResultsViewModel.Routes = responseModel.Data;
                return View(routesResultsViewModel);
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}