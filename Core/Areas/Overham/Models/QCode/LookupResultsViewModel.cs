﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.QCode
{
    public class LookupResultsViewModel
    {
        public string Tx { get; set; }
        public string Rx { get; set; }
        public string Authority { get; set; }
    }
}
