﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.QCode
{
    public class LookupViewModel
    {
        public string Code { get; set; }
    }
}
