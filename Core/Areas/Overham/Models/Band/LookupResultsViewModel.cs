﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Band
{
    public class LookupResultsViewModel
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Id { get; set; }
        public string Lower { get; set; }
        public string Upper { get; set; }
    }
}
