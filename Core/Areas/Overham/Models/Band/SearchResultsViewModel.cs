﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Band
{
    public class SearchResultsViewModel
    {
        public int[] BandId { get; set; }
    }
}
