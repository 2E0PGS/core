﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models
{
    public class ErrorViewModel
    {
        public string Message { get; set; }
    }
}
