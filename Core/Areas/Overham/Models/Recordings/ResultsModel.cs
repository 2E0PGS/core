﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Recordings
{
    public class ResultsModel
    {
        public string Duration { get; set; }
        public string HZFreq { get; set; }
        public string Id { get; set; }
        public DateTime WhenStart { get; set; }
    }
}
