﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Recordings
{
    public class SearchResultsViewModel
    {
        public int ResultCount { get; set; }
        public List<ResultsModel> Results { get; set; }
        public string EndpointCurrentVersionUrl { get; set; }
    }
}
