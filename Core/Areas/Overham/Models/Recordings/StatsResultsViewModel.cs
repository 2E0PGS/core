﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Recordings
{
    public class StatsResultsViewModel
    {
        public string UploadedCount { get; set; }
        public DateTime LastDump { get; set; }
        public string Available { get; set; }
        public string TotalCount { get; set; }
        public DateTime Earliest { get; set; }
        public DateTime Latest { get; set; }
        public string ModeAmCount { get; set; }
        public string ModeFmCount { get; set; }
        public string ModeLsbCount { get; set; }
        public string ModeUsbCount { get; set; }
        public string ModeDVCount { get; set; }
        public string ModeCWCount { get; set; }
    }
}
