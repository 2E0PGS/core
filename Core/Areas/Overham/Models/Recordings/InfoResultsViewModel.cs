﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Recordings
{
    public class InfoResultsViewModel
    {
        public string Mode { get; set; }
        public DateTime WhenStart { get; set; }
        public int Length { get; set; }
        public string Callsign { get; set; }
        public int HZFreq { get; set; }
        public string Model { get; set; }
        public int Band { get; set; }
        public string Maidenhead { get; set; }
        public string Direction { get; set; }
    }
}
