﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Index
{
    public class RoutesResultsViewModel
    {
        public List<string> Routes { get; set; }
    }
}
