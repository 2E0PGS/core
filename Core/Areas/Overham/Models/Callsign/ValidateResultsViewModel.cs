﻿using System.ComponentModel.DataAnnotations;

namespace Core.Areas.Overham.Models.Callsign
{
    public class ValidateResultsViewModel
    {
        public bool Valid { get; set; }
    }
}
