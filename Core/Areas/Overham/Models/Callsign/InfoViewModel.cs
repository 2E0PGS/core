﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Callsign
{
    public class InfoViewModel
    {
        public string Callsign { get; set; }
    }
}
