﻿using System.ComponentModel.DataAnnotations;

namespace Core.Areas.Overham.Models.Callsign
{
    public class InfoResultsViewModel
    {
        public string Region { get; set; }
        public string Country { get; set; }
        public string Callsign { get; set; }
        public string Type { get; set; }
        public string AmateurClass { get; set; }
    }
}
