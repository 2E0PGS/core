﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Resistor
{
    public class EncodeResultsViewModel
    {
        public string Band0Color { get; set; }
        public string Band1Color { get; set; }
        public string Band2Color { get; set; }
    }
}
