﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.Resistor
{
    public class EncodeViewModel
    {
        public string Ohms { get; set; }
    }
}
