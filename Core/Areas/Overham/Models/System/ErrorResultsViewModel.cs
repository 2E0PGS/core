﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.System
{
    public class ErrorResultsViewModel
    {
        public string Symbolic { get; set; }
        public string Message { get; set; }
        public string Id { get; set; }
    }
}
