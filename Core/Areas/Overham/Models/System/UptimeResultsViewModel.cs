﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.System
{
    public class UptimeResultsViewModel
    {
        public string Uptime { get; set; }
    }
}
