﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Areas.Overham.Models.System
{
    public class PingResultsViewModel
    {
        public string Response { get; set; }
    }
}
