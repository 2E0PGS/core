# core

A ASP.NET Core 2.0 MVC website which sits on my sub domain core.m3pgs.co.uk and is hosted on Linux for a working proof of concept of cross platform .NET.

It will primarily be hosting useful tools and clients for cool APIs such as overham written by Duncan Ross Palmer (2E0EOL).
